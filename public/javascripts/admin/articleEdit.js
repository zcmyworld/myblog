$(function() {

    KindEditor.ready(function(K) {
        window.editor = K.create('#editor_id');
        var route = "article/getSingleArticle";
        var articleId = $_GET['articleId'];
        var args = {
            articleId: articleId
        };
        ajaxPost(route, args, function(data) {
            $("#title").val(data.article[0].title);
            editor.html(data.article[0].content);
        })
    });
    $("#ensure").click(function() {
        var route = "article/updateArticle";
        var articleId = $_GET['articleId'];
        editor.sync();
        var content = document.getElementById('editor_id').value; // 原生API
        var title = $("#title").val();
        var args = {
            articleId: articleId,
            content: content,
            title: title
        }
        ajaxPost(route, args, function(data) {
            alert(data.error);
        })
    })
})

