$(function() {
    KindEditor.ready(function(K) {
        window.editor = K.create('#editor_id');
    });

    $("#test").click(function() {
        editor.sync();
        html = document.getElementById('editor_id').value;
        alert(html);
    })

    $('#myForm').ajaxForm();//监听
    $("#myForm").submit(function() {
        editor.sync();//同步
        html = document.getElementById('editor_id').value;//获取同步后的数据
        var args = {
            content: html
        }
        $(this).ajaxSubmit({
            data: args,
            type: 'post',
            success: function(data) {
                if (data.error) {
                    alert("上传失败");
                    return;
                }
                alert("上传成功");
            }
        })
        return false;
    })

    // $('#myForm').ajaxForm(function(data) {
    //     if (data.error) {
    //         alert("上传失败");
    //         return;
    //     }
    //     alert("上传成功");
    // });
})