$(function() {

    $("#download").click(function() {
        alert("download has click");
        $("#downloadwindow").attr("src", "images/upload.rar");
    })

    $(document).bind("contextmenu", function() {
        return false;
    });


    var currentMusicName; //当前播放的音乐名字
    var currentMusicNode = $("#playList").find(".music_element").first(); //当前播放的节点,默认为第一首
    var firstMusic = $("#playList").find(".music_element").first();
    var lastMusic = $("#playList").find(".music_element").last();
    var currentMusicIndex = 0;
    var playmode = 1; //播放模式，默认为顺序播放

    /*初始化*/
    var musicCount = $("#playList").children(".music_element").length; //歌曲数
    $("#playList").css("height", musicCount * 30 + 12); //设置歌曲列表长度
    currentMusicName = currentMusicNode.find(".music_element_name").html();
    $("#music_name").html(currentMusicName);

    if (currentMusicName == "棋魂") {
        currentMusicName = "qihun";
    }
    if (currentMusicName == "angel beats") {
        currentMusicName = "angelbeats";
    }
    if (currentMusicName == "仙侠世界") {
        currentMusicName = "xianxiashijie";
    }
    if (currentMusicName == "仙剑问情") {
        currentMusicName = "xianjianwenqing";
    }
    if (currentMusicName == "刀剑神域") {
        currentMusicName = "daojianshenyu";
    }
    // var musicsrc = "images/musics/" + currentMusicName + ".mp3";
    var musicsrc = "http://zcmyworld-images.qiniudn.com/" + currentMusicName + ".mp3"
    $("#audio").attr("src", musicsrc);

    //播放完成后
    $("#audio").bind('ended', function() {
        if (playmode == 1) {
            loopplayback();
        }
        if (playmode == 2) {
            shuffleplay();
        }
        if (playmode == 3) {
            singlecycleplay();
        }
    });
    $("#playmode").click(function() {
        $("#modechange").fadeToggle(200);
    })

    $(".modechange_element").click(function() {
        var modeName = $(this).html();
        if (modeName == '顺序') {
            playmode = 1;
            $("#playmode").html(modeName);
        }
        if (modeName == '单曲') {
            playmode = 3;
            $("#playmode").html(modeName);
        }
        $("#modechange").fadeOut(200);
    })
    //循环播放1
    function loopplayback() {
        if (currentMusicIndex < musicCount - 1) {
            currentMusicIndex++;
            currentMusicNode = currentMusicNode.next();
            selectThisMusic();
        } else {
            // alert("已经是最后一首");
            currentMusicIndex = 0;
            currentMusicNode = firstMusic;
            selectThisMusic();
        }
    }
    //随机播放2
    function shuffleplay() {

    }
    //单曲循环3
    function singlecycleplay() {
        selectThisMusic();
    }
    //

    //鼠标悬停样式
    $(".music_element").live('mouseover', function() {
        $(".music_element").css("color", "white");
        $(".music_element").css("font-size", "16px");
        $(this).css("color", "#03A9F4");
        $(currentMusicNode).css("font-size", "25px");
        $(currentMusicNode).css("color", "#03A9F4")
    })
    $(".music_element").live('mouseleave', function() {
        $(".music_element").css("color", "white");
        $(".music_element").css("font-size", "16px");
        $(currentMusicNode).css("font-size", "25px");
        $(currentMusicNode).css("color", "#03A9F4")
    })
    //选择音乐
    // $(".music_element").live('click', function() {
    //     currentMusicNode = $(this);
    //     currentMusicIndex = currentMusicNode.index();
    //     selectThisMusic();
    // })
    var isMouseMenu = false; //判断鼠标是否在右键菜单上
    $("#mousemenu").live("mouseover", function() {
        isMouseMenu = true;
    })
    $("#mousemenu").live("mouseleave", function() {
        isMouseMenu = false;
    })

    var downMusicName;

    $(".music_element").mousedown(function(e) {
        downMusicName = $(this).find(".music_element_name").html();
        if (e.which == 1) {
            currentMusicNode = $(this);
            currentMusicIndex = currentMusicNode.index();
            selectThisMusic();
        } else {
            $("#mousemenu").remove();
        }
        if (e.which == 3) {
            var st = $(document).scrollTop();
            isMouseMenu = true;
            var mouseX = e.pageX;
            var mouseY = e.pageY;
            $("#playList").append("<div id='mousemenu'><div class='mousemenu_download'><a download=''>下载</a></div></div>");
            $("#mousemenu").css("left", mouseX + 10);
            $("#mousemenu").css("top", mouseY + 10 - st);
        }
    })
    $("body").mousedown(function() {
        if (!isMouseMenu) {
            $("#mousemenu").remove();
        }
    })
    //下载
    $(".mousemenu_download").live("mouseover", function() {
        $(".mousemenu_download").css("color", "#03A9F4");
    })
    $(".mousemenu_download").live("mouseleave", function() {
        $(".mousemenu_download").css("color", "white");
    })
    $(".mousemenu_download").live("click", function() {
        isMouseMenu = false;
        $("#mousemenu").remove();
        if(currentMusicName =="棋魂"){
            currentMusicName = "qihun";
        }
        if(currentMusicName =="angel beats"){
            currentMusicName = "angelbeats";
        }
        if(currentMusicName =="仙侠世界"){
            currentMusicName = "xianxiashijie";
        }
        if(currentMusicName =="仙剑问情"){
            currentMusicName = "xianjianwenqing";
        }
        if(currentMusicName =="刀剑神域"){
            currentMusicName = "daojianshenyu";
        }
        // var musicsrc = "images/musics/" + currentMusicName + ".mp3";
        var musicsrc = "http://zcmyworld-images.qiniudn.com/"+currentMusicName+".mp3"
        $(this).find("a").attr("href", musicsrc);
    })
    //下一首
    $("#nextmusic").click(function() {
        if (currentMusicIndex < musicCount - 1) {
            currentMusicIndex++;
            currentMusicNode = currentMusicNode.next();
            selectThisMusic();
        } else {
            // alert("已经是最后一首");
            currentMusicIndex = 0;
            currentMusicNode = firstMusic;
            selectThisMusic();
        }
    })
    //上一首
    $("#prevmusic").click(function() {
        if (currentMusicIndex > 0) {
            currentMusicIndex--;
            currentMusicNode = currentMusicNode.prev();
            selectThisMusic();
        } else {
            // alert("已经是第一首");
            currentMusicIndex = musicCount - 1;
            currentMusicNode = lastMusic;
            selectThisMusic();
        }
    })

    //选择音乐
    function selectThisMusic() {
        currentMusicName = currentMusicNode.find(".music_element_name").html();
        $("#music_name").html(currentMusicName);
        if (currentMusicName == "棋魂") {
            currentMusicName = "qihun";
        }
        if (currentMusicName == "angel beats") {
            currentMusicName = "angelbeats";
        }
        if (currentMusicName == "仙侠世界") {
            currentMusicName = "xianxiashijie";
        }
        if (currentMusicName == "仙剑问情") {
            currentMusicName = "xianjianwenqing";
        }
        if (currentMusicName == "刀剑神域") {
            currentMusicName = "daojianshenyu";
        }
        // var musicsrc = "images/musics/" + currentMusicName + ".mp3";
        var musicsrc = "http://zcmyworld-images.qiniudn.com/" + currentMusicName + ".mp3"
        $("#audio").attr("src", musicsrc);
        $("#audio").attr("autoplay", "autoplay");
        $(".music_element").css("color", "white");
        $(".music_element").css("font-size", "16px");
        currentMusicNode.css("color", "#03A9F4");
        currentMusicNode.css("font-size", "25px");
    }

    // var audio = $("#audio");
    var audio;
    audio = document.getElementById('audio'); //不知道为什么用jquery获取不了
    $("#playstop").click(function() {
        if (audio.paused) {
            audio.play();
            $("#playstop").find("img").attr("src", "images/sysImg/stopsound.png");
            return;
        }
        audio.pause();
        $("#playstop").find("img").attr("src", "images/sysImg/playsound.png");
    })
    $("#bigsound").click(function() {
        var volume = audio.volume + 0.1;
        if (volume >= 1) {
            volume = 1;
        }
        audio.volume = volume;
    })
    $("#smallsound").click(function() {
        var volume = audio.volume - 0.1;
        if (volume <= 0) {
            volume = 0;
        }
        audio.volume = volume;
    })
    $("#stopsound").click(function() {
        if (audio.muted) {
            audio.muted = false;
        } else {
            audio.muted = true;
        }
    })
    $("#playtime").click(function() {
        audio.currentTime = 5;
    })
    $("#playrail").click(function(e) {
        var offset = $(this).offset(); //DIV在页面的位置  
        _x = e.pageX - offset.left; //获得鼠标指针离DIV元素左边界的距离  
        $("#music_name").html(_x);
        $("#playrailslider").css("margin-left", _x + 10);

    })

    /*键盘事件*/
    $(document).keydown(function(event) {
        // console.log(event.keyCode);
        // alert(event.keyCode);
        var keyVal = event.keyCode;
        if (keyVal == 39) {
            // alert("右");

        }
        if (keyVal == 37) {
            alert("左");
        }
    });
})