$(function() {
	getSingleArticle();
	prettyPrint();
	KindEditor.ready(function(K) {
		window.editor = K.create('#inputArea', {
			allowImageUpload: false,
			items: [
				'emoticons'
			]
		});
	});
	$("#issueComment").click(function() {
		editor.sync(); //同步
		var areaValue = document.getElementById('inputArea').value; //获取同步后的数据
		if(areaValue.length==0){
			alert("评论不能为空");
			return;
		}
		var route = "comment/commentAdd";
		var articleId = $_GET['articleId'];
		var args = {
			articleId: articleId,
			commentContent: areaValue
		}
		var number = $("#commentList").children(":last").prev().find(".number").html();

		var html = "<div class='commentList_ele'>" +
			"<div class='commentList_ele_title'>" +
			"<span>#" + (parseInt(number) + 1) + "楼</span>" +
			"<span class='commentTime'>" + new Date().toLocaleString() + "</span>" +
			"</div>" +
			"<div class='commentList_ele_content'>" +
			areaValue +
			"</div>" +
			"</div>" +
			"<div class='commentList_line'></div>";
		$("#commentList").append(html);
		editor.html('');
		ajaxPost(route, args, function(data) {
			if (data.error) {
				alert("系统错误");
			}
		})
	})
	$(window).scroll(function() {
		if ($(document).scrollTop() >= 300) {
			$("#toTop").show();
		} else {
			$("#toTop").hide();
		}
	})
	$("#toTop").click(function() {
		toTop();
	})
})

function getCommentList() {
	var articleId = $_GET['articleId'];
	var route = "comment/getCommentList";
	var args = {
		articleId: articleId
	}
	ajaxPost(route, args, function(data) {
		for (var i in data.commentList) {
			var time = new Date(data.commentList[i].commentTime).toLocaleString();
			var html = "<div class='commentList_ele'>" +
				"<div class='commentList_ele_title'>" +
				"<span>#<span class='number'>" + (parseInt(i) + 1) + "</span>楼</span>" +
				"<span class='commentTime'>" + time + "</span>" +
				"</div>" +
				"<div class='commentList_ele_content'>" +
				data.commentList[i].commentContent +
				"</div>" +
				"</div>" +
				"<div class='commentList_line'></div>";
			$("#commentList").append(html);
		}
	})
}

function getSingleArticle() {
	var articleId = $_GET['articleId'];
	var route = "article/getSingleArticle";
	var args = {
		articleId: articleId
	}
	ajaxPost(route, args, function(data) {
		$("#article_title").text(data.article[0].title);
		$("#article_time").text(new Date(data.article[0].time).toLocaleString());
		$("#article_content").html(data.article[0].content);
		getCommentList();
	})
}

function toTop() {
	var t = 0;
	$("body,html").animate({
		scrollTop: t
	}, 300)
}