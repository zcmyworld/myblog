/**/
var ip = window.location.hostname;
var port = window.location.port;

function ajaxPost(route, args, callback) {
	var url = "http://" + ip + ":" + port + "/" + route;
	$.ajax({
		url: url,
		type: "post",
		data: args,
		datatype: "json",
		error: function() {
			// alert("系统错误");
		},
		success: function(data) {
			callback(data);
		}
	});
};

//获取get请求的数据
var $_GET = (function() {
	var url = window.document.location.href.toString();
	var u = url.split("?");
	if (typeof(u[1]) == "string") {
		u = u[1].split("&");
		var get = {};
		for (var i in u) {
			var j = u[i].split("=");
			get [j[0]] = j[1];
		}
		return get;
	} else {
		return {};
	}
})();


$(function() {
	/*点击标题*/
	$("#blogName").click(function() {
		window.location.href = '/';
	})
	/*搜索框*/
	var pathname = window.location.pathname.substring(1);
	var currentNavPage; //当前页面
	var currentOnmouse; //当前鼠标在导航栏悬停位置
	var currentSelect; //当前选择的导航
	/*导航栏*/
	$(".nav_element").click(function() {
		var currentId = $(this).attr("id");
		currentNavPage = "#" + currentId;
		currentSelect = $(currentNavPage).html();
		if (currentSelect == '文章') {
			window.location.href = '/';
		}
		if (currentSelect == '相册') {
			window.location.href = '/photo';
		}
		if (currentSelect == '音乐') {
			window.location.href = '/music';
		}
		if (currentSelect == '作品') {
			window.location.href = '/exhibit';
		}
		if (currentSelect == '留言板') {
			window.location.href = '/message';
		}
		if (currentSelect == '关于我') {
			window.location.href = '/aboutme';
		}
	});
})