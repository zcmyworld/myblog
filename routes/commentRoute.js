/*
 * GET home page.
 */

var Constants = require('../utils/Constants');
var CommentDao = require('../dao/IndexDao').CommentDao;
module.exports = function(app) {


	app.post('/comment/commentAdd', function(req, res) {
		var commentContent = req.body.commentContent;
		var articleId = req.body.articleId;
		CommentDao.insertComment(commentContent, articleId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})

	app.post('/comment/getCommentList', function(req, res) {
		var articleId = req.body.articleId;
		CommentDao.getCommentList(articleId,function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				commentList: ret
			})
		})
	})

};