/*
 * GET home page.
 */
var Constants = require('../utils/Constants');
var fs = require('fs');

module.exports = function(app) {
	app.post('/upload', function(req, res) {
		for (var i in req.files) {
			if (req.files[i].size == 0) {
				fs.unlinkSync(req.files[i].path);
				res.send({
					error: 0
				})
			} else {
				var target_path = './public/images/titleImg/' + req.files[i].name;
				fs.renameSync(req.files[i].path, target_path);
				resPath = 'images/titleImg/' + req.files[i].name;
				res.send({
					"error": 0,
					"url": resPath
				});
			}
		}
	})
};