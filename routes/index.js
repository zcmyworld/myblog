
var pageRoute = require('./pageRoute');
var articleRoute = require('./articleRoute');
var uploadRoute = require('./uploadRoute');
var photoRoute = require('./photoRoute');
var messageRoute = require('./messageRoute');
var commentRoute = require('./commentRoute');
module.exports = function(app){
	pageRoute(app);
	articleRoute(app);
	uploadRoute(app);
	photoRoute(app);
	messageRoute(app);
	commentRoute(app);
};