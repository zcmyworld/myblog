var dbclient = require('./mysql/mysql').pool;
var PhotoDao = module.exports;


PhotoDao.insertPhoto = function(photoImgFileUrl, photoImgUrl, photoDes, callback) {
	var sql = 'insert into photo (photoImgFileUrl, photoImgUrl, photoDes) values (?,?,?)';
	var args = [photoImgFileUrl, photoImgUrl, photoDes];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});

}

PhotoDao.getPhotoList = function(callback) {
	var sql = 'select photoId,photoImgFileUrl,photoImgUrl,photoDes,photoImgTime from photo';
	var args = [];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

PhotoDao.getDataCount = function(callback) {
	var sql = 'select count(articleId) as articleCount from article';
	var args = [];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

PhotoDao.getSingleArticle = function(articleId, callback) {
	var sql = 'select articleId,title,titleImg,content,time from article where articleId = ?'
	var args = [articleId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}