var MessageDao = module.exports;
var dbclient = require('./mysql/mysql').pool;


MessageDao.insertMessage = function(content, callback) {
	var sql = 'insert into message (content) values (?)';
	var args = [content];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});

}

MessageDao.getMessageList = function(callback) {
	var sql = 'select messageId,content,messageTime from message';
	var args = [];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

MessageDao.getDataCount = function(callback) {
	var sql = 'select count(articleId) as articleCount from article';
	var args = [];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}