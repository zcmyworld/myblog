exports.ArticleDao = require('./ArticleDao');
exports.UploadDao = require('./UploadDao');
exports.PhotoDao = require('./PhotoDao');
exports.MessageDao = require('./MessageDao');
exports.CommentDao = require('./CommentDao');